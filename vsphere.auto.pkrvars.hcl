##################################################################################
# VARIABLES
##################################################################################
# Credentials

ssh_username                    = "ubuntu"
ssh_password                    = "ubuntu"

# vSphere Objects

vcenter_insecure_connection     = true
vcenter_server                  = "vcsa.sturla.uk"
vcenter_datacenter              = "Datacenter"
vcenter_host                    = "rs-esxi-02.sturla.uk"
vcenter_datastore               = "HPDS01"
vcenter_network                 = "Temporary"
vcenter_folder                  = "Templates/Base"