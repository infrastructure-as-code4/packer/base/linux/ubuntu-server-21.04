##################################################################################
# LOCALS
##################################################################################

locals {
  buildtime = formatdate("YYYY-MM-DD hh:mm ZZZ", timestamp())
}

##################################################################################
# SOURCE
##################################################################################

source "vsphere-iso" "ubuntu-server-2104" {
  vcenter_server = var.vcenter_server
  username = var.vcenter_username
  password = var.vcenter_password
  datacenter = var.vcenter_datacenter
  datastore = var.vcenter_datastore
  host = var.vcenter_host
  folder = var.vcenter_folder
  insecure_connection = var.vcenter_insecure_connection
  tools_upgrade_policy = true
  remove_cdrom = true
  convert_to_template = true
  guest_os_type = var.vm_guest_os_type
  vm_version = var.vm_version
  notes = "Build: ${local.buildtime}"
  vm_name = var.vm_name
  firmware = var.vm_firmware
  CPUs = var.vm_cpu_sockets
  cpu_cores = var.vm_cpu_cores
  CPU_hot_plug = false
  RAM = var.vm_mem_size
  RAM_hot_plug = false
  cdrom_type = var.vm_cdrom_type
  disk_controller_type = var.vm_disk_controller_type
  storage {
    disk_size = var.vm_disk_size
    disk_controller_index = 0
    disk_thin_provisioned = true
  }
  network_adapters {
    network = var.vcenter_network
    network_card = var.vm_network_card
  }
  iso_urls = var.iso_urls
  iso_paths = var.iso_paths
  iso_checksum = var.iso_checksum
  floppy_files = var.floppy_files
  floppy_label = "cidata"
  boot_order = "disk,cdrom"
  boot_wait = var.vm_boot_wait
  boot_command = var.vm_boot_command
  ip_wait_timeout = "20m"
  ssh_password = var.ssh_password
  ssh_username = var.ssh_username
  ssh_port = 22
  ssh_timeout = "30m"
  ssh_handshake_attempts = "100000"
  shutdown_command = "echo '${var.ssh_password}' | sudo -S -E shutdown -P now"
  shutdown_timeout = "15m"
}


##################################################################################
# BUILD
##################################################################################

build {
  sources = [
    "source.vsphere-iso.ubuntu-server-2104"
  ]

  provisioner "ansible" {
    playbook_file = "./ansible/main.yml"
    user = "ubuntu"
    extra_arguments = ["--extra-vars",  "ansible_sudo_pass=${var.ssh_password}"]
  }

  provisioner "shell" {
    execute_command = "echo '${var.ssh_password}' | {{.Vars}} sudo -S -E bash '{{.Path}}'"
    environment_vars = [
      "BUILD_USERNAME=${var.ssh_username}",
    ]
    scripts = var.shell_scripts
    expect_disconnect = true
  }
}
