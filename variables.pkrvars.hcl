##################################################################################
# VARIABLES
##################################################################################
floppy_files = ["./cdrom/meta-data", "./cdrom/user-data"]

# Virtual Machine Settings
vm_boot_command             = [
    "<esc><esc><esc><esc>e<wait>",
    "<del><del><del><del><del><del><del><del>",
    "<del><del><del><del><del><del><del><del>",
    "<del><del><del><del><del><del><del><del>",
    "<del><del><del><del><del><del><del><del>",
    "<del><del><del><del><del><del><del><del>",
    "<del><del><del><del><del><del><del><del>",
    "<del><del><del><del><del><del><del><del>",
    "<del><del><del><del><del><del><del><del>",
    "<del><del><del><del><del><del><del><del>",
    "<del><del><del><del><del><del><del><del>",
    "<del><del><del><del><del><del><del><del>",
    "<del><del><del><del><del><del><del><del>",
    "<del><del><del><del><del><del><del><del>",
    "<del><del><del><del><del><del><del><del>",
    "linux /casper/vmlinuz root=/dev/sr0 initrd=/casper/initrd autoinstall ipv6.disable=1<enter><wait>",
    "initrd /casper/initrd<enter><wait>",
    "boot<enter>",
    "<enter><f10><wait>"
]

vm_name                     = "Ubuntu Server 21.04 Template"
vm_guest_os_type            = "ubuntu64Guest"
vm_version                  = 14
vm_firmware                 = "bios"
vm_cdrom_type               = "sata"
vm_cpu_sockets              = 4
vm_cpu_cores                = 1
vm_mem_size                 = 8192
vm_disk_size                = 32000
vm_disk_controller_type     = ["pvscsi"]
vm_network_card             = "vmxnet3"
vm_boot_wait                = "2s"

# ISO Objects
iso_paths                   = ["[HPDS01] .ISOs/Linux/ubuntu-21.04-live-server-amd64.iso"]
iso_checksum                = "e4089c47104375b59951bad6c7b3ee5d9f6d80bfac4597e43a716bb8f5c1f3b0"
iso_urls                    = ["https://releases.ubuntu.com/21.04/ubuntu-21.04-live-server-amd64.iso"]

# Scripts
shell_scripts               = ["./scripts/generalize.sh"]
